<!DOCTYPE HTML>
<html>

	<head>
		<title>Buat Account</title>
	</head>
	
	<body>
		<h1>Buat Account Baru!</h1>
		
		<h2>Sign Up Form</h2>
				
		<form action='/submit' method='post'>
            @csrf
		<div id='name'>
			<label for='firstname'>First name:</label><br>
			<input type='text' id='firstname' name='firstname'><br>
			<label for='lastname'> Last name: </label><br>
			<input type='text' id='lastname' name='lastname'><br>
		</div>
		
		<div id='gender'>
			<p>Gender: </p>
			<input type='radio' id='male' name='gender' value='male'>
			<label for='male'>Male</label><br>
			<input type='radio' id='female' name='gender' value='female'>
			<label for='female'>Female</label><br>
			<input type='radio' id='other' name='gender' value='other'>
			<label for='other'>Other</label><br>
			
		</div><br>
		
		<div id='nationality'>
		<label for='nationality'>Kebangsaan: </label><br><br>
			<select name='nationality' id='nationality'>
				<option value='indonesia'>Indonesia</option>
				<option value='amerika'>Amerika</option>
				<option value='inggris'>Inggris</option>
			</select>
		</div>
		
		<div id='language'>
			<p>Language Spoken: </p>
			<input type='checkbox' id='bahasa' name='language' value='bahasa'>
			<label for='bahasa'>Bahasa Indonesia</label><br>
			<input type='checkbox' id='english' name='language' value='english'>
			<label for='english'>English</label><br>
			<input type='checkbox' id='other' name='language' value='other'>
			<label for='other'>Other</label><br>			
		</div>
		
		<div id='bio'>
			<p>Bio: </p>
			<textarea id='bio' name='bio' rows=5 cols=20></textarea>			
		</div>
		
		<div id='submit'>
			<input type='submit' value='submit'>
		</div>
		
		</form>
	
	</body>
	
</html>