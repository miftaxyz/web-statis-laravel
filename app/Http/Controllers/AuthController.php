<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('tugas.register');
    }

    public function submit(Request $request){
        $firstName = $request->firstname;
        $lastName   = $request->lastname;

        return view('tugas.home', compact('firstName', 'lastName'));
    }
}
